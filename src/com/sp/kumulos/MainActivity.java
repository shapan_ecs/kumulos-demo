package com.sp.kumulos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.kumulos.android.jsonclient.Kumulos;
import com.kumulos.android.jsonclient.ResponseHandler;

public class MainActivity extends Activity {
    static final String TAG = "MainActivity";

    // define our UI elements
    private TextView mUsernameField;
    private TextView mPasswordField;
    private ProgressDialog mProgress;
    private String uid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mUsernameField = (TextView) findViewById(R.id.username_field);
        mPasswordField = (TextView) findViewById(R.id.password_field);
        Kumulos.initWithAPIKeyAndSecretKey("8r15s1csbvvprwt7v0pgfshrwvy4jrf8", "kbvpkvv8", this);

    }
    // called by the 'Log In' button on the UI
    public void handleLogin(View v) {
        // show a loading progress dialog
        mProgress = ProgressDialog.show(MainActivity.this, "",
                "Signing in...", true);

        // get the username/password combination from the UI
        String username = mUsernameField.getText().toString();
        String password = mPasswordField.getText().toString();
        Log.v(TAG, "Logging in: " + username + ":" + password);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);

        Kumulos.call("login", params, new ResponseHandler() {
                @Override
                public void didCompleteWithResult(Object result) {
                    ArrayList<Object> list = (ArrayList<Object>) result;
                    LinkedHashMap<String, Object> res = (LinkedHashMap<String, Object>) list.get(0);
                    uid = (String)res.get("userID");
                    Log.v(TAG, "uid is : "+uid);
                    mProgress.cancel();
                 // tell the console and the user it was a success!
                    Toast.makeText(MainActivity.this,
                            "User login successful!", Toast.LENGTH_SHORT)
                            .show();
                    Intent i = new Intent(MainActivity.this, NoteActivity.class);
                    i.putExtra("uid", uid);
                    startActivity(i);
                }
                

                @Override
                public void onFailure(Throwable error) {
                    mProgress.cancel();
                    Log.v(TAG, "onfailure");
                    error.printStackTrace();
                    Toast.makeText(
                            MainActivity.this,
                            "Error Registering: "
                                    + error.getLocalizedMessage(),
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFinish() {
                    Log.v(TAG, "on finish");
                }

                @Override
                public void onStart() {
                    Log.v(TAG, "on start");
                }
                
        });

    }

    // called by the 'Sign Up' button on the UI
    public void handleSignUp(View v) {

        // show a loading progress dialog
        mProgress = ProgressDialog.show(MainActivity.this, "",
                "Signing up...", true);

        // get the username/password combination from the UI
        String username = mUsernameField.getText().toString();
        String password = mPasswordField.getText().toString();
        Log.v(TAG, "Signup: " + username + ":" + password);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);

        Kumulos.call("register", params, new ResponseHandler() {
                @Override
                public void didCompleteWithResult(Object result) {
                    uid = result.toString();
                    Log.v(TAG, "uid is : "+uid);
                    mProgress.cancel();
                 // tell the console and the user it was a success!
                    Toast.makeText(MainActivity.this,
                            "User signup successful!", Toast.LENGTH_SHORT)
                            .show();
                    Intent i = new Intent(MainActivity.this, NoteActivity.class);
                    i.putExtra("uid", uid);
                    startActivity(i);
                }

                @Override
                public void onFailure(Throwable error) {
                    mProgress.cancel();
                    Log.v(TAG, "onfailure");
                    error.printStackTrace();
                    Toast.makeText(
                            MainActivity.this,
                            "Error Registering: "
                                    + error.getLocalizedMessage(),
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFinish() {
                    Log.v(TAG, "on finish");
                }

                @Override
                public void onStart() {
                    Log.v(TAG, "on start");
                }
                
        });


    }



}
