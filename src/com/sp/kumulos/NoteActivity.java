package com.sp.kumulos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import com.kumulos.android.jsonclient.Kumulos;
import com.kumulos.android.jsonclient.ResponseHandler;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class NoteActivity extends Activity {
    String uid = null;

    private static final String TAG = "NoteActivity";
    private ProgressDialog mProgress;
    private ListView mListView;
    private ObjectAdapter mListAdapter;
    private TextView editText;
    
    public class ObjectAdapter extends ArrayAdapter<Object> {

        // define some vars
        int resource;
        String response;
        Context context;

        // initialize the adapter
        public ObjectAdapter(Context context, int resource, List<Object> items) {
            super(context, resource, items);

            // save the resource for later
            this.resource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LinearLayout rowView;

            Object obj = getItem(position);

            if (convertView == null) {

                rowView = new LinearLayout(getContext());
                String inflater = Context.LAYOUT_INFLATER_SERVICE;
                LayoutInflater vi;
                vi = (LayoutInflater) getContext().getSystemService(inflater);
                vi.inflate(resource, rowView, true);

            }

            else {
                rowView = (LinearLayout) convertView;
            }

            TextView titleText = (TextView) rowView
                    .findViewById(R.id.rowTextTitle);

            titleText.setText(obj.toString());
            return rowView;
        }

    }


    public void addItem(View v) {
        mProgress = ProgressDialog.show(NoteActivity.this, "",
                "Creating Note...", true);
        final String value = editText.getText().toString();
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("content", value);
        params.put("User", uid);
        Kumulos.call("createNote", params, new ResponseHandler() {
            @Override
            public void didCompleteWithResult(Object result) {
                if(value.length() > 20)
                    mListAdapter.insert(value.substring(0,20)+"..", 0);
                else
                    mListAdapter.insert(value, 0);
                mProgress.cancel();
                editText.setText("");
            }
            

            @Override
            public void onFailure(Throwable error) {
                mProgress.cancel();
                Log.v(TAG, "onfailure");
                error.printStackTrace();
                Toast.makeText(
                        NoteActivity.this,
                        "Error create notes: "
                                + error.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                Log.v(TAG, "on finish");
            }

            @Override
            public void onStart() {
                Log.v(TAG, "on start");
            }
            
    });

    }

    private void loadObjects() {

        // default to an empty adapter
        mListAdapter.clear();

        // show a progress dialog to the user
        mProgress = ProgressDialog.show(NoteActivity.this, "", "Loading...",
                true);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("User", uid);
        Kumulos.call("getNotes", params, new ResponseHandler() {
            @Override
            public void didCompleteWithResult(Object result) {
                ArrayList<Object> list = (ArrayList<Object>) result;
                for(Object o : list) {
                    LinkedHashMap<String, Object> res = (LinkedHashMap<String, Object>) o;
                    String content = (String)res.get("content");
                    if(content.length() > 20)
                        mListAdapter.add(content.substring(0,20)+"..");
                    else
                        mListAdapter.add(content);
                    Log.v(TAG, "uid is : "+uid);
                }
                
                mProgress.cancel();
            }
            

            @Override
            public void onFailure(Throwable error) {
                mProgress.cancel();
                Log.v(TAG, "onfailure");
                error.printStackTrace();
                Toast.makeText(
                        NoteActivity.this,
                        "Error getNotes: "
                                + error.getLocalizedMessage(),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                Log.v(TAG, "on finish");
            }

            @Override
            public void onStart() {
                Log.v(TAG, "on start");
            }
            
    });

    }

    // the user has clicked an item on the list.
    // we use this action to possibly delete the tapped object.
    // to confirm, we prompt the user with a dialog:
    public void onListItemClick(ListView l, View v, final int position,
            long id) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.note);
        mListAdapter = new ObjectAdapter(this, R.layout.row,
                new ArrayList<Object>());
        mListView = (ListView) this.findViewById(R.id.list);
        editText = (TextView) findViewById(R.id.editText);
        // set it to our view's list
        mListView.setAdapter(mListAdapter);
        uid = getIntent().getStringExtra("uid");
        Log.v(TAG, "NoteActivity : UID:"+ uid);
        // query for any previously-created objects
        this.loadObjects();

    }

}
